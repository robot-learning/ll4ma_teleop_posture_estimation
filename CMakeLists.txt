cmake_minimum_required(VERSION 2.8.3)
project(ll4ma_rula_ergonomics)

## Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS roscpp rospy sensor_msgs message_generation std_msgs geometry_msgs)

## Declare ROS messages and services


## Generate added messages and services


## Generate messages in the 'msg' folder
 add_message_files(
   FILES
   rula_score.msg

 )

generate_messages(DEPENDENCIES std_msgs)

## Declare a catkin package
catkin_package(LIBRARIES ll4ma_rula_ergonomics CATKIN_DEPENDS roscpp rospy sensor_msgs DEPENDS system_lib message_generation message_runtime )


## Build talker and listener
include_directories(${catkin_INCLUDE_DIRS})
